<?php
require "entity/Asociado.php";
require "entity/ImagenGaleria.php";
include __DIR__ . "/utils/utils.php";

$objImagenGallery = array();

for ($i=1; $i < 13; $i++) { 
    $objImagenGallery[$i] = new ImagenGaleria($i.'.jpg', 'imagen '.$i,rand(0,10000),rand(0,10000),rand(0,10000));
}

$asociados = array();
$asociados[] = new Asociado("Discord", "discordia.png","App de videoconferencia");
$asociados[] = new Asociado("Fabrica", "fabrica.png","fabrica");
$asociados[] = new Asociado("Huawei", "huawei.png","Empresa de moviles");
$asociados[] = new Asociado("Pedal", "pedal-de-arranque.png","Pedal de arranque");
$asociados[] = new Asociado("Ubuntu", "ubuntu.png","Sistema operativo Ubuntu");
$asociados[] = new Asociado("vimeo", "vimeo.png","Vimeo");


require "views/index.view.php";

