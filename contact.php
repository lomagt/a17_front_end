<?php
include __DIR__ . "/utils/utils.php";

$errores = array();
$nombre = '';
$email = '';
$mensaje = '';
$apellidos = '';
$error = '';
$datos = '';



if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_POST["nombre"])) {
        $nombre = htmlspecialchars(trim($_POST['nombre']));
    }

    if (isset($_POST["apellidos"])) {
        $apellidos = htmlspecialchars(trim($_POST['apellidos']));
    }

    if (isset($_POST["email"])) {
        $email = htmlspecialchars(trim($_POST['email']));
    }

    if (isset($_POST["sujeto"])) {
        $sujeto = htmlspecialchars(trim($_POST['sujeto']));
    }

    if (isset($_POST["mensaje"])) {
        $mensaje = htmlspecialchars(trim($_POST['mensaje']));
    }

    if (empty($nombre)) {
        $errores['nombre'] = "El campo First Name es obligatorio\n";
    }

    if (empty($email)) {
        $errores['email'] = "El campo Email es obligatorio\n";
    } else {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errores['email'] = "El campo Email no es válido\n";
        }
    }
    if (empty($sujeto)) {
        $errores['sujeto'] = "El campo subject es obligatorio\n";
    }

    if (haveError($errores)) {
        
        $error = errorInput($errores);
        
    } else {
        $datos = muestraDatos($nombre, $apellidos, $email, $sujeto, $mensaje);
        
    }
    
}


function haveError($arr)
{
    if (empty($arr)) {
        return false;
    } else {
        return true;
    }
}

function muestraDatos($n, $a, $e, $s, $m)
{
    $form = '';

    $form .= $n;
    $form .= ' ' . $a . "\n";
    $form .= $e . "\n";
    $form .= $s . "\n";
    $form .= $m . "\n";
    return $form;
}

function errorInput($arr1)
{
    $cadena = '';
    
    if(isset($arr1['nombre'])){
        $cadena .= $arr1['nombre'];
    }
    if(isset($arr1['email'])){
        $cadena .= $arr1['email'];
    }
    if(isset($arr1['sujeto'])){
        $cadena .= $arr1['sujeto'];
    }
    
    return $cadena;
}


require "views/contact.view.php";
