<?php
function esActiva($string)
{
    if (strpos($_SERVER['REQUEST_URI'], $string) !== false) {
        return true;
    } else {
        return false;
    }
}

function extraerLogo($arr)
{

    if(count($arr) <= 3){
        return $arr;
    }else {
        shuffle($arr);
        return array_slice($arr, 0, 3);
    }

}
